#=======================================================================================
#title           :DP.py
#description     : Generates random data and apply the HDP algorithm with contour display
#author          :Djekidel Mohamed Nadhir
#date            :2013/12/24
#version         :0.1
#usage           :python DP.py
#========================================================================================

import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import sys 
from Helper import *
import math



##########################################################################################
#  This method randomly generates some data points given the means and covariance matrix
#Paramters:
#   mues: a list of centers (each center is a list)
#   cov : covariance matrix
#   nbPoints: the number of points to generate in each cluster
###########################################################################################
def generateData(mues,cov,nbPoints):
   
    Data = []
    for mu in mues :        
        Data.append( np.matrix(np.random.multivariate_normal(mu,cov,nbPoints) ))    
    return Data

######################################################
#  This method is used to plot the data and their contour
######################################################
def plotData(ax,Data, mues):

    
    #for pts in Data:
    #    plt.scatter(pts[:,0],pts[:,1])       
    ax.scatter(Data[:,0].tolist(),Data[:,1].tolist(), color='#00A48A')

    x = np.arange(-15.0, 15.0, 0.025)
    y = np.arange(-15.0, 15.0, 0.025)
    X, Y = np.meshgrid(x, y)
    Z=0

    for mu in mues:
        zi = mlab.bivariate_normal(X, Y, 1.0, 1.0, mu[0], mu[1])
        Z += zi    

    CS = ax.contour(X, Y, Z)
    ax.clabel(CS, inline=1, fontsize=10)    
    

def main(argv):


    ITMAX = 100
    epsilon = 0.001

    mues = [ [-2,2], [1,1], [3,-3], [5,-10] ]
    cov =  [[1,0],
            [0,1]]

    Data = generateData(mues, cov, 200);

    Data_merged = Data[0]
    for dt in range(1,len(Data)):
        Data_merged = np.row_stack((Data_merged,Data[dt]) )
    
    K=1
    Theta = [Data_merged.mean(0)] #Take the mean as our starting point
    Z = [1]* (Data_merged.shape[0]) #All the data are assigned to cluster 1

    #plotData(Data_merged,mues)

    helper = Helper(Data_merged,Theta,0.1,K,Z)

    converge = 0
    it = 0
    D_K_alpha = []
    D_M_phi = []
    D_M_data = []
    nbCluster = []
    while(converge == 0 and it < ITMAX):
        helper.sample_p_z()
        helper.Sample_Theta()
        nbCluster.append(helper.K)
        it += 1
        E_K_n = np.sum([helper.alpha/(i -1 + helper.alpha) for i in range(Data_merged.shape[0])])
        D_K_alpha.append(helper.K - E_K_n)
        D_M_phi.append(np.sum([ np.sqrt(np.inner(phi,phi)) for phi in helper.Theta ] ))
        
        sum = 0
        for i in range(len(helper.Z)):
            y = helper.data[i,:]- helper.Theta[helper.Z[i]-1]
            sum += float(np.inner(y,y))
        D_M_data.append(sum)
        print("iteration %s: K= %s D(k,alpha)= %s D(phi)= %s  D(data)= %s\n" % (it-1,helper.K,D_K_alpha[-1],D_M_phi[-1],sum))
    

    fig = plt.figure()
    ax1 = plt.subplot2grid((4,2),(0,0))
    ax1.plot(range(1,len(D_K_alpha)+1), D_K_alpha, 'bo', linewidth=2,color="#348ABD")    
    ax1.plot(range(1,len(D_K_alpha)+1), D_K_alpha, '--', linewidth=0.5,color="#A60628") 
    plt.xlim([0,it+1])   
    plt.title(r'$D_M(K;\alpha)$')
    fig.add_subplot(ax1)

    ax2 = plt.subplot2grid((4,2),(1,0))
    ax2.plot(range(1,len(D_M_phi)+1), D_M_phi, 'bo', linewidth=2,color="#348ABD")
    ax2.plot(range(1,len(D_M_phi)+1), D_M_phi, '--', linewidth=0.5,color="#A60628")   
    plt.xlim([0,it+1])   
    plt.title(r'$D_M(\phi;\sigma)$')
    fig.add_subplot(ax2)

    ax3 = plt.subplot2grid((4,2),(2,0))
    ax3.plot(range(1,len(D_M_data)+1), D_M_data, 'bo', linewidth=2,color="#348ABD")
    ax3.plot(range(1,len(D_M_data)+1), D_M_data, '--', linewidth=0.5,color="#A60628")    
    plt.xlim([0,it+1])   
    plt.title(r'$D_M(D;z,\phi)$')
    fig.add_subplot(ax3)

    ax4 = plt.subplot2grid((4,2),(3,0))
    ax4.plot(range(1,len(nbCluster)+1), nbCluster, 'bo', linewidth=2,color="#348ABD")
    ax4.plot(range(1,len(nbCluster)+1), nbCluster, '--', linewidth=0.5,color="#A60628")
    plt.xlim([0,it+1])   
    ax4.set_xlabel("Iteration")
    plt.title("Number of clusters")
    fig.add_subplot(ax3)

    ax4 = plt.subplot2grid((4,2),(0,1), rowspan=3)
    plotData(ax4,helper.data,[phi[0] for phi in helper.Theta] )
    plt.title("Number of clusters = %s" % helper.K)
    fig.add_subplot(ax4)

    plt.show()
    


    
    

    return 0


if __name__ == "__main__":
    main(sys.argv[1:])
