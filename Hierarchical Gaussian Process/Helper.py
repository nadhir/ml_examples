###################################################################################
#  Author: Djekidel Mohamed Nadhir
#  Description: This helper class implementes functions to estimate the diffrent 
#               model probabilities
#
##################################################################################


import collections
import numpy as np


class Helper(object):
    """description of class"""
    
    Z = []  #Contains the index of each point
    K =1    #The initial number of clusters
    alpha = 0.1  
    Theta = []  #Contains the centers cordinate of each cluster
    data = []   
    p_w_z = []  #  |X| * |K| probability of a point in a cluster K


    def __init__(self,data,centers,alpha,nbTopics, assignments ):   
        self.data = data
        self.Z = assignments
        self.alpha = alpha  
        self.Theta = centers
        return   


    #####################################################################
    #  This function removes empty cluster indexes after estimating Z   #
    #####################################################################
    def compact_assignments(self,emptyPos):

        #Get the list of empty clusters
        emptyPos = np.sort(emptyPos)
        #Calculate how many empty clusters before the actual index of the point
        index_change = [sum(i < k for i in emptyPos) for k in self.Z]
        #Remove the empty clusters and adjust the other clusters index
        self.Z = self.Z - index_change
        self.K = np.max(self.Z)            
        return 0

    #####################################################################
    #  Calculates the probability of a assigning a word to a cluster    #
    #####################################################################
    def sample_p_z(self):

        nbPoints = self.data.shape[0]
        self.p_w_z = np.zeros((nbPoints,self.K +1))
        assignments = np.array(self.Z)
        

        for pts in range(nbPoints):
            freqencies = collections.Counter(assignments)                  
            clus_list = np.sort(list(freqencies.keys()))
            freq = np.zeros(self.K)           
            pos = [k-1 for k in clus_list if k < (self.K +1)]
            freq[pos] = np.array([freqencies[k] for k in clus_list if k < (self.K +1) ])  
                             

            my_clus = assignments[pts]
            freq[my_clus-1]-=1            
            p_z_k = freq[0:self.K] / (nbPoints-1 + self.alpha) 
            #We just consider the centers which have elments (because the number will change during the sampling)            
            p_x_z = [1/(2*np.pi) * np.exp( -0.5 * float( np.inner(self.data[pts,:]- mu,self.data[pts,:] - mu) ) ) for  mu in self.Theta]   
            p_z = list( p_z_k * p_x_z )            
            p_z_new = (self.alpha/(nbPoints-1 + self.alpha)) * 1/(4*np.pi) * np.exp(-0.25 * float(np.inner(self.data[pts,:],self.data[pts,:])) )
            p_z.extend([p_z_new])
            self.p_w_z[pts,:] = p_z / np.sum(p_z)

            #Sample Z from the calculated distribution
            assignments[pts] = np.random.choice(range(1, self.K +2),size=1,p= self.p_w_z[pts,:])            
        
        #Check which clusters are empty
        freq = collections.Counter(assignments)
        pos_empty = [i for i in range(1,len(list(freq.keys()))+1)  if i not in list(freq.keys())]
        if(len(pos_empty)>0):
            #Shrink the pwz table
            self.Z = assignments
            self.compact_assignments(pos_empty)
        else:
            self.Z =  assignments       
            self.K = np.max(assignments)
        return 0
    
     
    #####################################################################
    #  Estimates the centers postions                                   #
    #####################################################################
    def Sample_Theta(self):

       freq = collections.Counter(self.Z)  
       self.Theta =[0]* self.K

       for i in range(1,self.K + 1):
           c_k = freq[i]
           inK_pos = [idx for idx,x in enumerate(self.Z) if x==i]
           inK = self.data[inK_pos,:]
           muK = (inK.sum(0) / (c_k + 1)).tolist()[0]
           sigma_k = np.identity(2)/ (c_k + 1)   
           self.Theta[i-1] = np.random.multivariate_normal(muK,sigma_k,1)

       return 0
    
